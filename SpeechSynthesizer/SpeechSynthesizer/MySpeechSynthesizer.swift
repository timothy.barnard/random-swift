//
//  MySpeechSynthesizer.swift
//  SpeechSynthesizer
//
//  Created by Tims on 18/08/2019.
//  Copyright © 2019 Tims. All rights reserved.
//

import UIKit
import AVFoundation

final class MySpeechSynthesizer: NSObject {

    /// List of available voices
    public var availableVoices = AVSpeechSynthesisVoice.speechVoices()
    
    private var resultClosure: ((NSAttributedString) -> Void)?
    private let speechSynthesizer = AVSpeechSynthesizer()
    
    override init() {
        super.init()
        speechSynthesizer.delegate = self
    }
    
    
    /// Speaks the message passed in.
    ///
    /// - Parameters:
    ///   - message: string in which to speak
    ///   - voice: voice to use
    ///   - clousure: optional clouse to return back highlighted spoken string
    public func speak(with message: String, with voice: AVSpeechSynthesisVoice,
                      clousure: ((NSAttributedString) -> Void)? = nil) {
        let utterennce = AVSpeechUtterance(string: message)
        utterennce.voice = AVSpeechSynthesisVoice(identifier: voice.identifier)
        utterennce.rate = 0.35
        utterennce.pitchMultiplier = 1.3
        self.speechSynthesizer.speak(utterennce)
        self.resultClosure = clousure
    }
    
}

// MARK: - AVSpeechSynthesizerDelegate
extension MySpeechSynthesizer: AVSpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let mutableAttributedStirng = NSMutableAttributedString(string: utterance.speechString)
        mutableAttributedStirng.addAttribute(.foregroundColor, value: UIColor.red, range: characterRange)
        self.resultClosure?(mutableAttributedStirng)
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        self.resultClosure?(NSAttributedString(string: utterance.speechString))
    }
}
