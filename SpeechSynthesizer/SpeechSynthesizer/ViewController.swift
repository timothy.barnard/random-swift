//
//  ViewController.swift
//  SpeechSynthesizer
//
//  Created by Tims on 18/08/2019.
//  Copyright © 2019 Tims. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var titleLabel: UITextView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.font = .systemFont(ofSize: 15)
        $0.textColor = .white
        $0.backgroundColor = .black
        $0.returnKeyType = .done
        $0.delegate = self
        $0.text = "Welcome to my app. My name is Timothy Barnard"
        $0.textAlignment = .center
        return $0
    }(UITextView())
    
    lazy var button: UIButton = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.setTitle("Start", for: .normal)
        $0.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        $0.backgroundColor = .blue
        $0.setTitleColor(.white, for: .normal)
        $0.layer.cornerRadius = 10
        return $0
    }(UIButton())
    
    let mySpeechSynthesizer = MySpeechSynthesizer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

    // MARK: - Setup view
    private func setupView() {
        
        let stackView: UIStackView = {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.alignment = .center
            $0.axis = .vertical
            $0.spacing = 10
            return $0
        }(UIStackView())
        
        self.view.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(button)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.view.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            
            titleLabel.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            
            button.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.6),
            button.heightAnchor.constraint(equalToConstant: 50),
            ])
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        guard let message = self.titleLabel.text else {return}
        
        let alertViewController = UIAlertController(title: "Voice", message: "Please choose", preferredStyle: .actionSheet)
        for voice in self.mySpeechSynthesizer.availableVoices {
            let title: String = "\(voice.name) - \(voice.language)"
            let voiceAction = UIAlertAction(title: title, style: .default) { [weak self] action in
                self?.mySpeechSynthesizer.speak(with: message, with: voice) { [weak self] value in
                    self?.titleLabel.attributedText = value
                }
            }
            alertViewController.addAction(voiceAction)
        }
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        self.present(alertViewController, animated: true)
    }
}

extension ViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.last == "\n" {
            textView.resignFirstResponder()
        }
    }
}
